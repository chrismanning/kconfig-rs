use std::path::PathBuf;

use semver::Version;

fn main() {
    eprintln!("cargo:warning={:?}", std::env::vars().collect::<Vec<_>>());

    let mut config = cpp_build::Config::new();

    qt_setup(&mut config);
    kconfig_setup(&mut config);

    config.build("src/lib.rs");
}

fn kconfig_setup(config: &mut cpp_build::Config) {
    let (version, include_path, library_path) = probe_kconfig().unwrap();

    const LIB_NAME: &str = "ConfigCore";

    println!("cargo:rustc-link-search={}", library_path.to_string_lossy());

    config.include(
        kde_frameworks::get_lib_include_path(LIB_NAME, version.major, &include_path).unwrap(),
    );
    kde_frameworks::link_lib(LIB_NAME, version.major).unwrap();

    const LEAST_VERSION: u64 = 66;
    kde_frameworks::set_version_cfg(version, LEAST_VERSION);
}

fn qt_setup(config: &mut cpp_build::Config) -> Version {
    let qt_include_path = std::env::var("DEP_QT_INCLUDE_PATH").unwrap();
    let qt_library_path = std::env::var("DEP_QT_LIBRARY_PATH").unwrap();
    let qt_version = std::env::var("DEP_QT_VERSION")
        .unwrap()
        .parse::<Version>()
        .expect("Parsing Qt version failed");

    if cfg!(target_os = "macos") {
        config.flag("-F");
        config.flag(&qt_library_path);
    }

    if qt_version >= Version::new(6, 0, 0) {
        config.flag_if_supported("-std=c++17");
        config.flag_if_supported("/std:c++17");
        config.flag_if_supported("/Zc:__cplusplus");
    }

    config.include(&qt_include_path);

    // Include qtcore
    config.include(&format!("{}/{}", qt_include_path, "QtCore"));

    qt_version
}

fn probe_kconfig() -> Result<(Version, PathBuf, PathBuf), &'static str> {
    const LIBRARY_NAME: &str = "KCONFIG";

    if let Some(x) = kde_frameworks::check_env_variables(LIBRARY_NAME) {
        return Ok(x);
    }

    kde_frameworks::check_kf5_config()
}
