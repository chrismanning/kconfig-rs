use kconfig::kemailsettings::{KEMailSettings, Setting};

#[test]
fn basic() {
    let mut kemail = KEMailSettings::default();
    const DEFAULT_NAME: &str = "abc";

    kemail.set_default(DEFAULT_NAME.into());
    assert_eq!(kemail.default_profile_name(), DEFAULT_NAME.into());

    kemail.set_setting(Setting::ClientProgram, DEFAULT_NAME.into());
    assert_eq!(
        kemail.get_setting(Setting::ClientProgram),
        DEFAULT_NAME.into()
    );

    kemail.set_profile(DEFAULT_NAME.into());
    assert_eq!(kemail.profiles().len(), 2);
}
