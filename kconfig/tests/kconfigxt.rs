use std::thread;

#[cfg(feature = "kconfigxt")]
use kconfig::kcoreconfigskeleton::KCoreConfigSkeleton;
#[cfg(feature = "kconfigxt")]
use kconfig_impl::kcfgc;
#[cfg(feature = "kconfigxt")]
use kconfig_impl::kcfgc_inline;

#[test]
#[cfg(feature = "kconfigxt")]
fn basic() {
    kcfgc_inline!(
        r#"
            StructName=Test1
            File=test1.kcfg"#
    );

    let x = Test1 {
        _config: KCoreConfigSkeleton::default(),
        width: 100,
        height: 200,
    };

    Test1::default();

    assert_eq!(x.get_width(), 100);
    assert_eq!(x.get_height(), 200);
}

#[test]
#[cfg(feature = "kconfigxt")]
fn mutators() {
    kcfgc_inline!(
        r#"
            StructName=Test1
            File=test1.kcfg
            MemberVaiables=public
            Mutators=true"#
    );

    let mut x = Test1 {
        _config: KCoreConfigSkeleton::default(),
        width: 100,
        height: 200,
    };

    assert_eq!(x.get_width(), 100);
    x.set_width(200);
    assert_eq!(x.get_width(), 200);
}

#[test]
#[cfg(feature = "kconfigxt")]
fn default_value() {
    kcfgc_inline!(
        r#"
            StructName=Test1
            File=test1.kcfg
            MemberVaiables=crate
            DefaultValueGetters=true"#
    );

    assert_eq!(Test1::get_default_width(), 600);
    assert_eq!(Test1::get_default_height(), 600);
}

#[test]
#[cfg(feature = "kconfigxt")]
fn singleton() {
    kcfgc_inline!(
        r#"
            StructName=Test1
            File=test1.kcfg
            Singleton=true"#
    );

    let handle1 = thread::spawn(|| Test1::singleton().width);
    let handle2 = thread::spawn(|| Test1::singleton().width);

    assert_eq!(handle1.join().unwrap(), handle2.join().unwrap());
}

#[test]
#[cfg(feature = "kconfigxt")]
fn kcfgc_macro() {
    kcfgc!("test1.kcfgc");

    let handle1 = thread::spawn(|| Test1::singleton().width);
    let handle2 = thread::spawn(|| Test1::singleton().width);

    assert_eq!(handle1.join().unwrap(), handle2.join().unwrap());
}
