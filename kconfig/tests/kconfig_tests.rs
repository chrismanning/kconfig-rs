use kconfig::kconfig::{KConfig, KConfigTrait, OpenFlags};
use qttypes::{QStandardPathLocation, QString, QStringList};

#[test]
fn basic() {
    let mut config = KConfig::new(
        QString::default(),
        OpenFlags::SIMPLE_CONFIG,
        QStandardPathLocation::GenericConfigLocation,
    );
    assert_eq!(
        config.location_type(),
        QStandardPathLocation::GenericConfigLocation
    );
    assert_eq!(config.open_flags(), OpenFlags::SIMPLE_CONFIG);
    assert_eq!(config.name(), QString::default());

    assert_eq!(config.additional_config_sources().len(), 0);

    let mut l = QStringList::new();
    l.push("abc".into());
    l.push("def".into());
    config.add_config_sources(l);
    assert_eq!(config.additional_config_sources().len(), 2);

    let locale = QString::from("ja");
    assert!(config.set_locale(locale.clone()));
    assert_eq!(config.locale(), locale.clone());

    assert!(!config.read_defaults());
    config.set_read_defaults(true);
    assert!(config.read_defaults());
}

#[test]
fn open_flags() {
    assert_eq!(
        OpenFlags::FULL_CONFIG,
        OpenFlags::INCLUDE_GLOBALS | OpenFlags::CASCADE_CONFIG
    );
    assert_eq!(OpenFlags::NO_CASCADE, OpenFlags::INCLUDE_GLOBALS);
    assert_eq!(OpenFlags::NO_GLOBALS, OpenFlags::CASCADE_CONFIG);
}
