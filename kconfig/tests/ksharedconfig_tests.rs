use kconfig::{
    kconfig::OpenFlags,
    ksharedconfig::KSharedConfig,
    ksharedconfig::KSharedConfigPtr,
    prelude::{KConfig, KConfigTrait},
};
use qttypes::QStandardPathLocation;
use std::thread;

#[test]
fn single_tread() {
    let config1 = KSharedConfig::open_config(
        "abc",
        OpenFlags::SIMPLE_CONFIG,
        QStandardPathLocation::GenericConfigLocation,
    )
    .unwrap();
    let config2 = KSharedConfig::open_config(
        "abc",
        OpenFlags::SIMPLE_CONFIG,
        QStandardPathLocation::GenericConfigLocation,
    )
    .unwrap();

    assert_eq!(config1, config2);
}

#[test]
fn multi_thread() {
    let config1 = KSharedConfig::open_config(
        "abc",
        OpenFlags::SIMPLE_CONFIG,
        QStandardPathLocation::GenericConfigLocation,
    )
    .unwrap();

    let config2 = thread::spawn(|| {
        KSharedConfig::open_config(
            "abc",
            OpenFlags::SIMPLE_CONFIG,
            QStandardPathLocation::GenericConfigLocation,
        )
        .unwrap()
    })
    .join()
    .unwrap();

    assert_eq!(config1, config2);
}

#[test]
fn default_test() {
    let shared_config = KSharedConfigPtr::default();
    let config = KConfig::default();

    assert_eq!(config.name(), shared_config.name());
}
