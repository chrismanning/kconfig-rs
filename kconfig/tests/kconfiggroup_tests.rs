use kconfig::kconfig::{KConfig, OpenFlags};
use kconfig::kconfigbase::{KConfigBase, WriteConfigFlags};
use qmetaobject::QVariant;
use qttypes::{QStandardPathLocation, QString, QStringList};

#[test]
fn basic() {
    let mut config = KConfig::new(
        QString::default(),
        OpenFlags::SIMPLE_CONFIG,
        QStandardPathLocation::GenericConfigLocation,
    );
    let mut general = config.group("General");

    let mut g1 = general.group("G1");
    assert!(!g1.exists());
    g1.write_qvariant_entry("abc", true.into(), WriteConfigFlags::NORMAL)
        .unwrap();
    assert!(g1.exists());
    unsafe {
        general.delete_group("G1", WriteConfigFlags::NORMAL);
    }
    assert!(!g1.exists());
    assert_eq!(g1.name(), QString::from("G1"));
}

#[test]
fn read_write_qstring() {
    let mut config = KConfig::new(
        QString::default(),
        OpenFlags::SIMPLE_CONFIG,
        QStandardPathLocation::GenericConfigLocation,
    );
    let mut general = config.group("General");

    let key = "key1";
    let val = "val";

    assert!(general.read_qstring_entry(key).is_err());

    general
        .write_string_entry(key, val, WriteConfigFlags::NORMAL)
        .unwrap();

    assert_eq!(general.read_qstring_entry(key).unwrap(), val.into());
}

#[test]
fn read_write_qvariant() {
    let mut config = KConfig::new(
        QString::default(),
        OpenFlags::SIMPLE_CONFIG,
        QStandardPathLocation::GenericConfigLocation,
    );

    assert!(QVariant::default().is_null());

    let mut general = config.group("General");

    let key = "key1";
    let val: u32 = 11;

    assert_eq!(
        general.read_qvariant_entry(key, val.into()).unwrap(),
        val.into()
    );

    general
        .write_qvariant_entry(key, QVariant::from(val), WriteConfigFlags::NORMAL)
        .unwrap();

    assert!(general.has_key(key).unwrap());
    assert_eq!(
        general
            .read_qvariant_entry(key, QVariant::from(10 as u32))
            .unwrap(),
        QVariant::from(val)
    );
}

#[test]
fn keys() {
    //let mut config = KConfig::new(
    //    QString::default(),
    //    OpenFlags::SIMPLE_CONFIG,
    //    QStandardPathLocation::GenericConfigLocation,
    //);
    //let mut general = config.group("General");

    //let key1 = QString::from("key1");
    //let key2 = QString::from("key2");

    //assert!(general.read_qvariant_entry(key1.to_string()).is_none());

    //general
    //    .write_qvariant_entry(
    //        key1.to_string(),
    //        QString::from("abc").to_qvariant(),
    //        WriteConfigFlags::NORMAL,
    //    )
    //    .unwrap();

    //assert_eq!(general.key_list().len(), 1);
    //assert!(general.has_key(key1.clone()));
    //assert!(!general.has_key(key2.clone()));
    //assert_eq!(
    //    QString::from_qvariant(
    //        general
    //            .read_qvariant_entry(key1.to_string())
    //            .unwrap_or(false.into())
    //    )
    //    .unwrap(),
    //    QString::from("abc")
    //);

    //let val = QString::from("ABC");
    //general
    //    .write_qstring_entry(key2.to_string(), val.clone(), WriteConfigFlags::NORMAL)
    //    .unwrap();
    //assert_eq!(general.key_list().len(), 2);
    //assert_eq!(general.read_qstring_entry(key2.to_string()), Some(val));
}

#[test]
fn path_entry() {
    let mut config = KConfig::new(
        QString::default(),
        OpenFlags::SIMPLE_CONFIG,
        QStandardPathLocation::GenericConfigLocation,
    );
    let mut general = config.group("General");
    assert!(general.is_valid());

    let path = QString::from("abc");
    let key = QString::from("abc");
    general
        .write_path_entry(key.to_string(), path.clone(), WriteConfigFlags::NORMAL)
        .unwrap();
    assert_eq!(general.read_path_entry(key.to_string()).unwrap(), path);

    let mut paths = QStringList::new();
    paths.push("xyz".into());
    paths.push("abc".into());

    let key = QString::from("xdg");
    general
        .write_xdg_list_entry(key.to_string(), paths.clone(), WriteConfigFlags::NORMAL)
        .unwrap();
    assert_eq!(
        general.read_xdg_list_entry(key.to_string()).unwrap().len(),
        paths.len()
    );
}

#[test]
fn group_after_delete() {
    let mut config = KConfig::new(
        QString::default(),
        OpenFlags::SIMPLE_CONFIG,
        QStandardPathLocation::GenericConfigLocation,
    );
    let mut general = config.group("General");
    general
        .write_qvariant_entry("abc", true.into(), WriteConfigFlags::NORMAL)
        .unwrap();
    assert!(general.exists());

    unsafe {
        config.delete_group(general.name(), WriteConfigFlags::NORMAL);
    }
    assert!(!general.exists());
}
