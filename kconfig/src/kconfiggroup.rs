//! Contains constructs defined in[`KConfigGroup`][header] header
//!
//! [header]: https://api.kde.org/frameworks/kconfig/html/classKConfigGroup.html

use std::{
    ffi::{CStr, CString},
    fmt,
};

use cpp::{cpp, cpp_class};
use qttypes::{QByteArray, QString, QStringList, QVariant};

use crate::kconfigbase::{AccessMode, KConfigBase, KConfigBaseImpl, WriteConfigFlags};
use crate::{errors, helpers};

cpp! {{
    #include <KConfigGroup>
    #include <KConfigBase>
}}

cpp_class!(
    /// Struct representing [`KConfigGroup`][class] class.
    ///
    /// [class]: https://api.kde.org/frameworks/kconfig/html/classKConfigGroup.html
    #[derive(Default, Clone)]
    pub unsafe struct KConfigGroup as "KConfigGroup"
);

impl fmt::Debug for KConfigGroup {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("KConfig")
            .field("name", &self.name())
            .finish()
    }
}

// Contains impl methods
impl KConfigGroup {
    fn read_qstring_entry_impl(&self, key: &CStr) -> errors::Result<QString> {
        let key_ptr = key.as_ptr();
        let r = cpp!(unsafe [self as "const KConfigGroup*", key_ptr as "const char *"] -> QString as "QString" {
            return self->readEntry(key_ptr, QString());
        });
        helpers::qstring_entry_check(key, r)
    }

    fn read_qvariant_entry_impl(&self, key: &CStr, adefault: QVariant) -> QVariant {
        let key = key.as_ptr();
        cpp!(unsafe [self as "const KConfigGroup *", key as "const char *", adefault as "QVariant"] -> QVariant as "QVariant" {
            return self->readEntry(key, adefault);
        })
    }

    fn write_qstring_entry_impl(&mut self, key: &CStr, value: QString, pflags: WriteConfigFlags) {
        let key = key.as_ptr();
        cpp!(unsafe [self as "KConfigGroup *", key as "const char*", value as "QString", pflags as "KConfigBase::WriteConfigFlag"] {
            self->writeEntry(key, value, pflags);
        })
    }

    fn write_qvariant_entry_impl(&mut self, key: &CStr, value: QVariant, pflags: WriteConfigFlags) {
        let key = key.as_ptr();
        cpp!(unsafe [self as "KConfigGroup *", key as "const char*", value as "QVariant", pflags as "KConfigBase::WriteConfigFlag"] {
            self->writeEntry(key, value, pflags);
        })
    }

    fn delete_entry_impl(&mut self, key: &CStr, pflags: WriteConfigFlags) {
        let key = key.as_ptr();
        cpp!(unsafe [self as "KConfigGroup*", key as "const char*", pflags as "KConfigBase::WriteConfigFlag"] {
            self->deleteEntry(key, pflags);
        });
    }

    fn has_key_impl(&self, key: &CStr) -> bool {
        let key = key.as_ptr();
        cpp!(unsafe [self as "const KConfigGroup*", key as "const char*"] -> bool as "bool" {
            return self->hasKey(key);
        })
    }

    fn is_entry_immutable_impl(&self, key: &CStr) -> bool {
        let key = key.as_ptr();
        cpp!(unsafe [self as "const KConfigGroup*", key as "const char*"] -> bool as "bool" {
            return self->isEntryImmutable(key);
        })
    }

    fn read_entry_untranslated_impl(&self, key: &CStr) -> errors::Result<QString> {
        let key_ptr = key.as_ptr();
        let s = cpp!(unsafe [self as "const KConfigGroup*", key_ptr as "const char*"] -> QString as "QString" {
            return self->readEntryUntranslated(key_ptr, QString());
        });
        helpers::qstring_entry_check(key, s)
    }

    fn revert_to_default_impl(&mut self, key: &CStr, flags: WriteConfigFlags) {
        let key = key.as_ptr();
        cpp!(unsafe [self as "KConfigGroup*", key as "const char*", flags as "KConfigBase::WriteConfigFlags"] {
            self->revertToDefault(key, flags);
        })
    }

    fn has_default_impl(&self, key: &CStr) -> bool {
        let key = key.as_ptr();
        cpp!(unsafe [self as "const KConfigGroup*", key as "const char*"] -> bool as "bool" {
            return self->hasDefault(key);
        })
    }

    fn write_path_entry_impl(&mut self, key: &CStr, path: QString, flags: WriteConfigFlags) {
        let key = key.as_ptr();
        cpp!(unsafe [self as "KConfigGroup*", key as "const char*", path as "QString", flags as "KConfigBase::WriteConfigFlags"] {
            self->writePathEntry(key, path, flags);
        })
    }

    fn read_xdg_list_entry_impl(&self, key: &CStr) -> errors::Result<QStringList> {
        let key_ptr = key.as_ptr();
        let l = cpp!(unsafe [self as "const KConfigGroup*", key_ptr as "const char*"] -> QStringList as "QStringList" {
            return self->readXdgListEntry(key_ptr, QStringList());
        });
        helpers::qstringlist_entry_check(key, l)
    }

    fn write_xdg_list_entry_impl(
        &mut self,
        key: &CStr,
        value: QStringList,
        flags: WriteConfigFlags,
    ) {
        let key = key.as_ptr();
        cpp!(unsafe [self as "KConfigGroup*", key as "const char*", value as "QStringList", flags as "KConfigBase::WriteConfigFlags"] {
            self->writeXdgListEntry(key, value, flags);
        })
    }

    fn read_path_entry_impl(&self, key: &CStr) -> errors::Result<QString> {
        let key_ptr = key.as_ptr();
        let s = cpp!(unsafe [self as "const KConfigGroup*", key_ptr as "const char*"] -> QString as "QString" {
            return self->readPathEntry(key_ptr, QString());
        });
        helpers::qstring_entry_check(key, s)
    }
}

impl KConfigGroup {
    /// Reads the qvariant value of an entry specified by key in the current group.
    pub fn read_qvariant_entry<K: Into<Vec<u8>>>(
        &self,
        key: K,
        adefault: QVariant,
    ) -> errors::Result<QVariant> {
        Ok(self.read_qvariant_entry_impl(&CString::new(key)?, adefault))
    }

    /// Reads the string value of an entry specified by key in the current group.
    pub fn read_qstring_entry<K: Into<Vec<u8>>>(&self, key: K) -> errors::Result<QString> {
        self.read_qstring_entry_impl(&CString::new(key)?)
    }

    /// Writes a value to the configuration object.
    pub fn write_qvariant_entry<K: Into<Vec<u8>>>(
        &mut self,
        key: K,
        value: QVariant,
        pflags: WriteConfigFlags,
    ) -> errors::Result<()> {
        Ok(self.write_qvariant_entry_impl(&CString::new(key)?, value, pflags))
    }

    /// Write a QString value to the configuration object.
    pub fn write_string_entry<K, V>(
        &mut self,
        key: K,
        value: V,
        pflags: WriteConfigFlags,
    ) -> errors::Result<()>
    where
        K: Into<Vec<u8>>,
        V: Into<QString>,
    {
        Ok(self.write_qstring_entry_impl(&CString::new(key)?, value.into(), pflags))
    }

    /// Deletes the entry specified by pKey in the current group.
    /// This also hides system wide defaults.
    pub fn delete_entry<K: Into<Vec<u8>>>(
        &mut self,
        key: K,
        pflags: WriteConfigFlags,
    ) -> errors::Result<()> {
        Ok(self.delete_entry_impl(&CString::new(key)?, pflags))
    }

    /// Checks whether the key has an entry in this group.
    pub fn has_key<K: Into<Vec<u8>>>(&self, key: K) -> Result<bool, errors::Error> {
        Ok(self.has_key_impl(&CString::new(key)?))
    }

    /// Check whether the containing KConfig object actually contains a group with this name.
    pub fn exists(&self) -> bool {
        cpp!(unsafe [self as "const KConfigGroup*"] -> bool as "bool" {
            return self->exists();
        })
    }

    /// Whether a default is specified for an entry in either the system wide configuration file or the global KDE config file.
    pub fn has_default<K: Into<Vec<u8>>>(&self, key: K) -> errors::Result<bool> {
        Ok(self.has_default_impl(&CString::new(key)?))
    }

    /// Returns a list of keys this group contains.
    pub fn key_list(&self) -> QStringList {
        cpp!(unsafe [self as "const KConfigGroup*"] -> QStringList as "QStringList" {
            return self->keyList();
        })
    }

    /// The name of this group.
    /// The root group is named "<default>".
    pub fn name(&self) -> QString {
        cpp!(unsafe [self as "const KConfigGroup*"] -> QString as "QString" {
            return self->name();
        })
    }

    /// Writes a file path to the configuration
    /// If the path is located under $HOME, the user's home directory is replaced with $HOME in the persistent storage.
    pub fn write_path_entry<K: Into<Vec<u8>>>(
        &mut self,
        key: K,
        path: QString,
        flags: WriteConfigFlags,
    ) -> errors::Result<()> {
        Ok(self.write_path_entry_impl(&CString::new(key)?, path, flags))
    }

    /// Reads a path.
    pub fn read_path_entry<K: Into<Vec<u8>>>(&self, key: K) -> errors::Result<QString> {
        self.read_path_entry_impl(&CString::new(key)?)
    }

    /// Writes a list of strings to the config object, following XDG desktop entry spec separator semantics.
    pub fn write_xdg_list_entry<K: Into<Vec<u8>>>(
        &mut self,
        key: K,
        value: QStringList,
        flags: WriteConfigFlags,
    ) -> errors::Result<()> {
        Ok(self.write_xdg_list_entry_impl(&CString::new(key)?, value, flags))
    }

    /// Reads a list of strings from the config object, following XDG desktop entry spec separator semantics.
    pub fn read_xdg_list_entry<K: Into<Vec<u8>>>(&self, key: K) -> errors::Result<QStringList> {
        self.read_xdg_list_entry_impl(&CString::new(key)?)
    }

    /// Reverts an entry to the default settings.
    /// Reverts the entry with key key in the current group in the application specific config file to either the system wide
    /// (default) value or the value specified in the global KDE config file.
    /// To revert entries in the global KDE config file, the global KDE config file should be opened explicitly in a separate config object.
    pub fn revert_to_default<K: Into<Vec<u8>>>(
        &mut self,
        key: K,
        flags: WriteConfigFlags,
    ) -> errors::Result<()> {
        Ok(self.revert_to_default_impl(&CString::new(key)?, flags))
    }

    /// Reads an untranslated string entry.
    /// You should not normally need to use this.
    pub fn read_entry_untranslated<K: Into<Vec<u8>>>(&self, key: K) -> errors::Result<QString> {
        self.read_entry_untranslated_impl(&CString::new(key)?)
    }

    // TODO: Maybe we don't need this in Rust
    /// Whether the group is valid. A group is invalid if it was constructed without arguments.
    /// You should not call any functions on an invalid group.
    pub fn is_valid(&self) -> bool {
        cpp!(unsafe [self as "const KConfigGroup*"] -> bool as "bool" {
            return self->isValid();
        })
    }

    /// If [`is_immutable()`](Self::is_immutable()) returns true, then this method will return true for all inputs.
    pub fn is_entry_immutable<K: Into<Vec<u8>>>(&self, key: K) -> errors::Result<bool> {
        Ok(self.is_entry_immutable_impl(&CString::new(key)?))
    }
}

impl KConfigBase for KConfigGroup {
    fn access_mode(&self) -> AccessMode {
        cpp!(unsafe [self as "const KConfigGroup*"] -> AccessMode as "KConfigBase::AccessMode" {
            return self->accessMode();
        })
    }

    fn mark_as_clean(&mut self) {
        cpp!(unsafe [self as "KConfigGroup*"] {
            self->markAsClean();
        });
    }

    fn sync(&mut self) -> bool {
        cpp!(unsafe [self as "KConfigGroup*"] -> bool as "bool" {
            return self->sync();
        })
    }

    fn is_immutable(&self) -> bool {
        cpp!(unsafe [self as "const KConfigHolder*"] -> bool as "bool" {
            return self->d->isImmutable();
        })
    }
}

impl KConfigBaseImpl for KConfigGroup {
    unsafe fn delete_group_impl(&mut self, group: QByteArray, flags: WriteConfigFlags) {
        cpp!(unsafe [self as "KConfigGroup *", group as "QByteArray", flags as "KConfigBase::WriteConfigFlags"] {
            self->deleteGroup(group, flags);
        });
    }

    fn group_impl(&mut self, group: QByteArray) -> KConfigGroup {
        cpp!(unsafe [self as "KConfigGroup *", group as "QByteArray"] -> KConfigGroup as "KConfigGroup" {
            return self->group(group);
        })
    }

    fn has_group_impl(&self, group: QByteArray) -> bool {
        cpp!(unsafe [self as "const KConfigGroup*", group as "QByteArray"] -> bool as "bool" {
            return self->hasGroup(group);
        })
    }

    fn group_list_impl(&self) -> QStringList {
        cpp!(unsafe [self as "const KConfigGroup*"] -> QStringList as "QStringList" {
            return self->groupList();
        })
    }
}
