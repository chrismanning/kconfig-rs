//! Contains constructs present [`KConfigBase`][header] header.
//!
//! [header]: https://api.kde.org/frameworks/kconfig/html/classKConfigBase.html

use crate::kconfiggroup::KConfigGroup;
use bitflags::bitflags;
use qttypes::{QByteArray, QStringList};

bitflags! {
    /// Flags to control write entry.
    #[repr(C)]
    pub struct WriteConfigFlags: u32 {
        /// Save this entry when saving the config object.
        const PERSISTENT = 0x01;
        /// Save the entry to the global KDE config file instead of the application specific config file.
        const GLOBAL = 0x02;
        /// Add the locale tag to the key when writing it.
        const LOCALIZED = 0x04;
        /// Notify remote KConfigWatchers of changes (requires DBus support) Implied persistent.
        /// Since 5.51
        const NOTIFY = 0x08 | Self::PERSISTENT.bits;
        /// Save the entry to the application specific config file without a locale tag. This is
        /// the default.
        const NORMAL = Self::PERSISTENT.bits;
    }
}

/// Possible return values for [`KConfigBase::access_mode()`].
#[repr(C)]
#[derive(Debug, PartialEq, Eq)]
pub enum AccessMode {
    /// The application-specific config file could not be opened neither read-write nor read-only
    NoAccess,
    /// The application-specific config file is opened read-only, but not read-write.
    ReadOnly,
    /// The application-specific config file is opened read-write
    ReadWrite,
}

pub trait KConfigBaseImpl {
    unsafe fn delete_group_impl(&mut self, group: QByteArray, flags: WriteConfigFlags);

    fn group_impl(&mut self, group: QByteArray) -> KConfigGroup;

    fn has_group_impl(&self, group: QByteArray) -> bool;

    fn group_list_impl(&self) -> QStringList;
}

/// Trait representing methods from [`KConfigBase`][class] class.
///
/// [class]: https://api.kde.org/frameworks/kconfig/html/classKConfigBase.html
pub trait KConfigBase: KConfigBaseImpl {
    /// Returns the access mode of the app-config object.
    fn access_mode(&self) -> AccessMode;

    /// This marks group as deleted in the config object. This effectively removes any cascaded values from config files earlier in the stack.
    /// This function is unsafe to call since it can make an in-scope `KConfigGroup` invalid.
    unsafe fn delete_group<T>(&mut self, group: T, flags: WriteConfigFlags)
    where
        T: Into<QByteArray>,
    {
        self.delete_group_impl(group.into(), flags)
    }

    /// Returns an object for the named subgroup. The group is not complemetely created untill an
    /// entry is added to the group (i.e. empty group is not returned by
    /// [`group_list()`](`KConfigBase::group_list()`))
    /// * `group` - the group to open. Pass an empty string here to the KConfig object to obtain a handle on the root group.
    fn group<T>(&mut self, group: T) -> KConfigGroup
    where
        T: Into<QByteArray>,
    {
        self.group_impl(group.into())
    }

    /// Returns a list of groups that are known about.
    fn group_list(&self) -> QStringList {
        self.group_list_impl()
    }

    /// Returns true if the specified group is known about.
    fn has_group<T>(&self, group: T) -> bool
    where
        T: Into<QByteArray>,
    {
        self.has_group_impl(group.into())
    }

    /// Reset the dirty flags of all entries in the entry map, so the values will not be written to disk on a later call to [`sync()`](Self::sync()).
    fn mark_as_clean(&mut self);

    /// Syncs the configuration object that this group belongs to.
    /// Unrelated concurrent changes to the same file are merged and thus not overwritten. Note however, that this object is not automatically updated with those changes.
    fn sync(&mut self) -> bool;

    /// Checks whether this configuration object can be modified.
    fn is_immutable(&self) -> bool;
}
