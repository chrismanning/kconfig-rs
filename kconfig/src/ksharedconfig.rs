//! Contains constructs defined in[`KSharedConfig`][header] header
//!
//! [header]: https://api.kde.org/frameworks/kconfig/html/classKSharedConfig.html

use std::sync::{Arc, Mutex};

use lazy_static::lazy_static;
use qttypes::{QStandardPathLocation, QString};

use crate::{
    errors::Error,
    kconfig::KConfig,
    kconfig::{KConfigTrait, OpenFlags},
};

/// Represents `KSharedConfigPtr`
pub type KSharedConfigPtr = Arc<KConfig>;

#[derive(Default)]
struct GlobalSharedConfigList {
    main_config: Option<KSharedConfigPtr>,
    list: Vec<KSharedConfigPtr>,
}

impl GlobalSharedConfigList {
    fn check_main_config(&mut self, config: KSharedConfigPtr) {
        if config.name().is_empty()
            && config.open_flags() == OpenFlags::FULL_CONFIG
            && config.location_type() == QStandardPathLocation::GenericConfigLocation
        {
            self.main_config = Some(config)
        }
    }

    fn check_object(
        &self,
        file_name: QString,
        mode: OpenFlags,
        path_type: QStandardPathLocation,
    ) -> Option<KSharedConfigPtr> {
        for i in &self.list {
            if i.name() == file_name && i.open_flags() == mode && i.location_type() == path_type {
                return Some(i.clone());
            }
        }
        None
    }

    pub fn create_object(
        &mut self,
        file_name: QString,
        mode: OpenFlags,
        path_type: QStandardPathLocation,
    ) -> KSharedConfigPtr {
        let config = Arc::new(KConfig::new(file_name, mode, path_type));
        self.list.push(config.clone());

        self.check_main_config(config.clone());

        config.clone()
    }
}

/// Provides [`KSharedConfig`][class] functioality in Rust.
/// This does not wrap a C++ class but rather is implemented completely in Rust.
///
/// [class]: https://api.kde.org/frameworks/kconfig/html/classKSharedConfig.html
pub struct KSharedConfig {
    _default: (),
}

impl KSharedConfig {
    // TODO: Check if config is Read Only.
    /// Creates a KSharedConfig object to manipulate a configuration file. This method is
    /// thread-safe.
    pub fn open_config<T: Into<QString>>(
        file: T,
        mode: OpenFlags,
        path_type: QStandardPathLocation,
    ) -> Result<KSharedConfigPtr, Error> {
        let mut file: QString = file.into();

        #[cfg(kf_5_93)]
        if file.is_empty() {
            file = KConfig::main_config_name();
        }

        #[cfg(not(kf_5_93))]
        if file.is_empty() {
            panic!("File name needs to be specified for KConfig version < 5.93");
        }

        lazy_static! {
            static ref GLOBALE_SHARED_CONFIG_LIST: Arc<Mutex<GlobalSharedConfigList>> =
                Arc::new(Mutex::new(GlobalSharedConfigList::default()));
        }

        let mut list = GLOBALE_SHARED_CONFIG_LIST.lock()?;

        if let Some(x) = list.check_object(file.clone(), mode, path_type) {
            return Ok(x);
        }

        Ok(list.create_object(file, mode, path_type))
    }

    /// Creates a KSharedConfig object to manipulate a configuration file suitable for storing state information.
    pub fn open_state_config<T: Into<QString>>(file: T) -> Result<KSharedConfigPtr, Error> {
        let mut file: QString = file.into();

        #[cfg(feature = "qmetaobject")]
        if file.is_empty() {
            use qmetaobject::qtcore::core_application::QCoreApplication;

            file = QCoreApplication::application_name() + QString::from("staterc");
        }

        #[cfg(not(feature = "qmetaobject"))]
        if file.is_empty() {
            panic!("Please enable qmetaobject feature to open default state config.");
        }

        Self::open_config(
            file,
            OpenFlags::SIMPLE_CONFIG,
            QStandardPathLocation::AppDataLocation,
        )
    }
}
