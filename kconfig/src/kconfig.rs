//! Contains constructs defined in[`KConfig`][header] header
//!
//! [header]: https://api.kde.org/frameworks/kconfig/html/classKConfig.html

use crate::kconfigbase::{AccessMode, KConfigBase, KConfigBaseImpl, WriteConfigFlags};
use crate::kconfiggroup::KConfigGroup;

use bitflags::bitflags;
use cpp::{cpp, cpp_class};
use qttypes::{QByteArray, QStandardPathLocation, QString, QStringList};
use std::fmt;

cpp! {{
    #include<KConfig>
    #include<memory>

    struct KConfigHolder {
        std::unique_ptr<KConfig> d;

        KConfigHolder(const QString &file=QString(), KConfig::OpenFlags mode=KConfig::FullConfig, QStandardPaths::StandardLocation type=QStandardPaths::GenericConfigLocation): d(new KConfig(file, mode, type)) {}
        KConfigHolder(const QString &file, const QString &backend, QStandardPaths::StandardLocation type=QStandardPaths::GenericConfigLocation): d(new KConfig(file, backend, type)) {}
    };
}}

bitflags! {
    /// Determines how the system-wide and user's global settings will affect the reading of the configuration.
    /// This is a bitfag. Thus it is possible to pass options like `OpenFlags::INCLUDE_GLOBALS |
    /// OpenFlags::CASCADE_CONFIG`
    #[repr(C)]
    pub struct OpenFlags: u32 {
        /// Blend kdeglobals into the config object.
        const INCLUDE_GLOBALS = 0x01;
        /// Cascade to system-wide config files.
        const CASCADE_CONFIG = 0x02;
        /// Just a single config file.
        const SIMPLE_CONFIG = 0x00;
        /// Include user's globals, but omit system settings.
        const NO_CASCADE = Self::INCLUDE_GLOBALS.bits;
        /// Cascade to system settings, but omit user's globals.
        const NO_GLOBALS = Self::CASCADE_CONFIG.bits;
        /// Fully-fledged config, including globals and cascading to system settings.
        const FULL_CONFIG = Self::INCLUDE_GLOBALS.bits | Self::CASCADE_CONFIG.bits;
    }
}

cpp_class!(
    /// Struct representing [`KConfig`][class] class.
    ///
    /// [class]: https://api.kde.org/frameworks/kconfig/html/classKConfig.html
    #[derive(Default)]
    pub unsafe struct KConfig as "KConfigHolder"
);

impl KConfig {
    /// Create a new KConfig Object
    pub fn new<T: Into<QString>>(
        file: T,
        mode: OpenFlags,
        path_type: QStandardPathLocation,
    ) -> Self {
        let file: QString = file.into();
        cpp!(unsafe [file as "QString", mode as "KConfig::OpenFlags", path_type as "QStandardPaths::StandardLocation"] -> KConfig as "KConfigHolder" {
            return KConfigHolder(file, mode, path_type);
        })
    }

    /// Create a new KConfig object with custom backend
    pub fn with_backend<T: Into<QString>>(
        file: T,
        backend: T,
        path_type: QStandardPathLocation,
    ) -> Self {
        let file: QString = file.into();
        let backend: QString = backend.into();
        cpp!(unsafe [file as "QString", backend as "QString", path_type as "QStandardPaths::StandardLocation"] -> KConfig as "KConfigHolder" {
            return KConfigHolder(file, backend, path_type);
        })
    }

    /// Create a KConfig Object with a specific name for the file.
    pub fn with_file<T: Into<QString>>(file: T) -> Self {
        Self::new(
            file.into(),
            OpenFlags::FULL_CONFIG,
            QStandardPathLocation::GenericConfigLocation,
        )
    }

    #[cfg(kf_5_93)]
    /// Get the main application config name.
    pub fn main_config_name() -> QString {
        cpp!(unsafe [] -> QString as "QString" {
            #if KCONFIG_VERSION >= QT_VERSION_CHECK(5, 93, 0)
            return KConfig::mainConfigName();
            #endif
        })
    }
}

impl PartialEq for KConfig {
    fn eq(&self, other: &Self) -> bool {
        self.name() == other.name()
            && self.location_type() == other.location_type()
            && self.open_flags() == other.open_flags()
    }
}

impl fmt::Debug for KConfig {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("KConfig")
            .field("name", &self.name())
            .field("flags", &self.open_flags())
            .field("location", &self.location_type())
            .finish()
    }
}

impl KConfigTrait for KConfig {
    fn is_dirty(&self) -> bool {
        cpp!(unsafe [self as "const KConfigHolder*"] -> bool as "bool" {
            return self->d->isDirty();
        })
    }

    fn is_config_writable(&self, warn_user: bool) -> bool {
        cpp!(unsafe [self as "const KConfigHolder*", warn_user as "bool"] -> bool as "bool" {
            return self->d->isConfigWritable(warn_user);
        })
    }

    fn open_flags(&self) -> OpenFlags {
        cpp!(unsafe [self as "const KConfigHolder*"] -> OpenFlags as "KConfig::OpenFlags" {
            return self->d->openFlags();
        })
    }

    fn reparse_configuration(&mut self) {
        cpp!(unsafe [self as "KConfigHolder*"] {
            self->d->reparseConfiguration();
        });
    }

    fn set_read_defaults(&mut self, b: bool) {
        cpp!(unsafe [self as "KConfigHolder*", b as "bool"] {
            self->d->setReadDefaults(b);
        });
    }

    fn read_defaults(&self) -> bool {
        cpp!(unsafe [self as "const KConfigHolder*"] -> bool as "bool" {
            return self->d->readDefaults();
        })
    }

    fn location_type(&self) -> QStandardPathLocation {
        cpp!(unsafe [self as "const KConfigHolder*"] -> QStandardPathLocation as "QStandardPaths::StandardLocation" {
            return self->d->locationType();
        })
    }
}

impl KConfigBase for KConfig {
    fn access_mode(&self) -> AccessMode {
        cpp!(unsafe [self as "const KConfigHolder*"] -> AccessMode as "KConfigBase::AccessMode" {
            return self->d->accessMode();
        })
    }

    fn mark_as_clean(&mut self) {
        cpp!(unsafe [self as "KConfigHolder*"] {
            self->d->markAsClean();
        });
    }

    fn sync(&mut self) -> bool {
        cpp!(unsafe [self as "KConfigHolder*"] -> bool as "bool" {
            return self->d->sync();
        })
    }

    fn is_immutable(&self) -> bool {
        cpp!(unsafe [self as "const KConfigHolder*"] -> bool as "bool" {
            return self->d->isImmutable();
        })
    }
}

impl KConfigBaseImpl for KConfig {
    unsafe fn delete_group_impl(&mut self, group: QByteArray, flags: WriteConfigFlags) {
        cpp!(unsafe [self as "KConfigHolder *", group as "QByteArray", flags as "KConfigBase::WriteConfigFlags"] {
            self->d->deleteGroup(group, flags);
        });
    }

    fn group_impl(&mut self, group: QByteArray) -> KConfigGroup {
        cpp!(unsafe [self as "KConfigHolder *", group as "QByteArray"] -> KConfigGroup as "KConfigGroup" {
            return self->d->group(group);
        })
    }

    fn has_group_impl(&self, group: QByteArray) -> bool {
        cpp!(unsafe [self as "const KConfigHolder*", group as "QByteArray"] -> bool as "bool" {
            return self->d->hasGroup(group);
        })
    }

    fn group_list_impl(&self) -> QStringList {
        cpp!(unsafe [self as "const KConfigHolder*"] -> QStringList as "QStringList" {
            return self->d->groupList();
        })
    }
}

impl KConfigImpl for KConfig {
    fn add_config_sources_impl(&mut self, sources: QStringList) {
        cpp!(unsafe [self as "KConfigHolder*", sources as "QStringList"] {
            self->d->addConfigSources(sources);
        });
    }

    fn additional_config_sources_impl(&self) -> QStringList {
        cpp!(unsafe [self as "const KConfigHolder*"] -> QStringList as "QStringList" {
            return self->d->additionalConfigSources();
        })
    }

    fn check_update_impl(&mut self, id: QString, update_file: QString) {
        cpp!(unsafe [self as "KConfigHolder*", id as "QString", update_file as "QString"] {
            self->d->checkUpdate(id, update_file);
        });
    }

    fn set_locale_impl(&mut self, locale: QString) -> bool {
        cpp!(unsafe [self as "KConfigHolder*", locale as "QString"] -> bool as "bool" {
            return self->d->setLocale(locale);
        })
    }

    fn locale_impl(&self) -> QString {
        cpp!(unsafe [self as "const KConfigHolder*"] -> QString as "QString" {
            return self->d->locale();
        })
    }

    fn name_impl(&self) -> QString {
        cpp!(unsafe [self as "const KConfigHolder*"] -> QString as "QString" {
            return self->d->name();
        })
    }
}

pub trait KConfigImpl {
    fn add_config_sources_impl(&mut self, sources: QStringList);

    fn additional_config_sources_impl(&self) -> QStringList;

    fn check_update_impl(&mut self, id: QString, update_file: QString);

    fn set_locale_impl(&mut self, locale: QString) -> bool;

    fn locale_impl(&self) -> QString;

    fn name_impl(&self) -> QString;
}

/// Trait representing methods from [`KConfig`](https://api.kde.org/frameworks/kconfig/html/classKConfig.html) class.
pub trait KConfigTrait: KConfigImpl {
    /// Sets the name of the application config file.
    fn set_main_config_name<T: Into<QString>>(name: T) {
        let name: QString = name.into();
        cpp!(unsafe [name as "QString"] {
            KConfig::setMainConfigName(name);
        })
    }

    /// Returns true if sync has any changes to write out.
    fn is_dirty(&self) -> bool;

    /// Returns the filename used to store the configuration.
    fn name(&self) -> QString {
        self.name_impl()
    }

    /// Whether the configuration can be written to.
    /// The most likely cause for this method returning false is that the user does not have write permission for the configuration file.
    /// # Argumenst
    /// * `warn_user` - whether to show a warning message to the user if the configuration cannot be written to
    fn is_config_writable(&self, warn_user: bool) -> bool;

    /// Returns the flags this object was opened with.
    fn open_flags(&self) -> OpenFlags;

    /// Updates the state of this object to match the persistent storage.
    /// Note that if this object has pending changes, this method will call [`sync()`](KConfigBase::sync()) first so as not to lose those changes.
    fn reparse_configuration(&mut self);

    /// Adds the list of configuration sources to the merge stack.
    /// Currently only files are accepted as configuration sources.
    /// The first entry in sources is treated as the most general and will be overridden by the second entry.
    /// The settings in the final entry in sources will override all the other sources provided in the list.
    /// The state is automatically updated by this method, so there is no need to call [`reparse_configuration()`](Self::reparse_configuration()).
    fn add_config_sources(&mut self, sources: QStringList) {
        self.add_config_sources_impl(sources)
    }

    /// Returns a list of the additional configuration sources used in this object.
    fn additional_config_sources(&self) -> QStringList {
        self.additional_config_sources_impl()
    }

    /// Returns the current locale.
    fn locale(&self) -> QString {
        self.locale_impl()
    }

    /// Set Locale
    fn set_locale<T>(&mut self, locale: T) -> bool
    where
        T: Into<QString>,
    {
        self.set_locale_impl(locale.into())
    }

    /// When set, all readEntry calls return the system-wide (default) values instead of the user's settings.
    /// This is off by default.
    fn set_read_defaults(&mut self, b: bool);

    /// `true` if the system-wide defaults will be read instead of the user's settings
    fn read_defaults(&self) -> bool;

    /// Returns the standard location enum passed to the constructor.
    fn location_type(&self) -> QStandardPathLocation;

    /// Ensures that the configuration file contains a certain update.
    /// If the configuration file does not contain the update `id` as contained in updateFile,
    /// `kconf_update` is run to update the configuration file.
    /// If you install config update files with critical fixes you may wish to use this method to verify that a
    /// critical update has indeed been performed to catch the case where a user restores an old config file from
    /// backup that has not been updated yet.
    /// # Arguments
    /// * `id` : the update to check
    /// * `update_file` : 	the file containing the update
    fn check_update<T>(&mut self, id: T, update_file: T)
    where
        T: Into<QString>,
    {
        self.check_update_impl(id.into(), update_file.into())
    }
}
