use std::str::FromStr;

use kconfig_compiler::{kcfg_to_tokenstream, kcfgc::Kcfgc};
use syn::{parse_macro_input, ExprLit, Lit};

#[proc_macro]
pub fn kcfg_inline(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input = parse_macro_input!(input as ExprLit);
    match input.lit {
        Lit::Str(xml) => kcfg_to_tokenstream(&Kcfgc::default(), xml.value().as_str())
            .unwrap()
            .into(),
        _ => panic!("Not valid Input"),
    }
}

#[proc_macro]
pub fn kcfgc_inline(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input = parse_macro_input!(input as ExprLit);
    let x = match input.lit {
        Lit::Str(kcfgc) => Kcfgc::from_str(kcfgc.value().as_str()).unwrap(),
        _ => panic!("Invalid Input for kcfgc"),
    };
    let kcfg = x.get_kcfg_contents().unwrap();
    kcfg_to_tokenstream(&x, &kcfg).unwrap().into()
}

#[proc_macro]
pub fn kcfgc(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input = parse_macro_input!(input as ExprLit);
    let x = match input.lit {
        Lit::Str(file) => Kcfgc::from_file(file.value().as_str()).unwrap(),
        _ => panic!("Invalid Input for kcfgc"),
    };
    let kcfg = x.get_kcfg_contents().unwrap();
    kcfg_to_tokenstream(&x, &kcfg).unwrap().into()
}
