use super::representation;
use crate::errors::{kcfg_errrors, Error, KcfgSerializeError};
use std::str::FromStr;

impl FromStr for representation::Kcfg {
    type Err = Error;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let xmltree = roxmltree::Document::parse(s)?;
        Ok(representation::Kcfg::try_from(xmltree.root_element())?)
    }
}

impl<'a, 'input: 'a> TryFrom<roxmltree::Node<'a, 'input>> for representation::Kcfg {
    type Error = KcfgSerializeError;

    fn try_from(xmlnode: roxmltree::Node) -> Result<Self, Self::Error> {
        let groups = xmlnode
            .children()
            .filter(|x| x.tag_name().name() == "group")
            .map(|x| representation::Group::try_from(x))
            .collect::<Result<Vec<representation::Group>, Self::Error>>()?;

        let file_name = xmlnode
            .children()
            .find(|x| x.tag_name().name() == "kcfgfile")
            .map(|x| x.attribute("name"));

        let file_name = if let Some(Some(x)) = file_name {
            x.to_string()
        } else {
            todo!()
        };

        Ok(Self::new(file_name, groups))
    }
}

impl<'a, 'input: 'a> TryFrom<roxmltree::Node<'a, 'input>> for representation::Group {
    type Error = KcfgSerializeError;

    fn try_from(xmlnode: roxmltree::Node) -> Result<Self, Self::Error> {
        let name = match xmlnode.attribute("name") {
            Some(attr) => attr.to_string(),
            None => {
                return Err(Self::Error::NameAttributeAbsent(
                    xmlnode.tag_name().name().to_string(),
                ))
            }
        };
        let entries = xmlnode
            .children()
            .filter(|x| x.tag_name().name() == "entry")
            .map(|x| representation::KcfgEntry::try_from(x))
            .collect::<Result<Vec<representation::KcfgEntry>, Self::Error>>()?;
        Ok(Self::new(name, entries))
    }
}

impl<'a, 'input: 'a> TryFrom<roxmltree::Node<'a, 'input>> for representation::KcfgEntry {
    type Error = KcfgSerializeError;

    fn try_from(xmlnode: roxmltree::Node) -> Result<Self, Self::Error> {
        let name = match xmlnode.attribute("name") {
            Some(attr) => attr.to_string(),
            None => {
                return Err(Self::Error::NameAttributeAbsent(
                    xmlnode.tag_name().name().to_string(),
                ))
            }
        };
        let entry_type = representation::EntryType::try_from(xmlnode)?;
        Ok(Self::new(name, entry_type))
    }
}

impl<'a, 'input: 'a> TryFrom<roxmltree::Node<'a, 'input>> for representation::EntryType {
    type Error = KcfgSerializeError;

    fn try_from(xmlnode: roxmltree::Node) -> Result<Self, Self::Error> {
        match xmlnode.attribute("type") {
            Some(attr) => match attr {
                "Int" => Ok(Self::Int(representation::BasicType::try_from(
                    xmlnode.children(),
                )?)),
                "UInt" => Ok(Self::UInt(representation::BasicType::try_from(
                    xmlnode.children(),
                )?)),
                "Int64" => Ok(Self::Int64(representation::BasicType::try_from(
                    xmlnode.children(),
                )?)),
                "UInt64" => Ok(Self::UInt64(representation::BasicType::try_from(
                    xmlnode.children(),
                )?)),
                "Double" => Ok(Self::Double(representation::BasicType::try_from(
                    xmlnode.children(),
                )?)),
                "Bool" => Ok(Self::Bool(representation::BasicType::try_from(
                    xmlnode.children(),
                )?)),
                "Enum" => Ok(Self::Enum(representation::EnumEntry::try_from(
                    xmlnode.children(),
                )?)),
                _ => Err(kcfg_errrors::EntryTypeError::UnsupportedType.into()),
            },
            None => Err(kcfg_errrors::EntryTypeError::TypeAttributeAbsent.into()),
        }
    }
}

impl<'a, 'input, T> TryFrom<roxmltree::Children<'a, 'input>> for representation::BasicType<T>
where
    'input: 'a,
    T: FromStr,
    kcfg_errrors::EntryTypeError: From<<T as FromStr>::Err>,
{
    type Error = kcfg_errrors::EntryTypeError;

    fn try_from(xmltree: roxmltree::Children) -> Result<Self, Self::Error> {
        basic_type_try_from(xmltree)
    }
}

fn basic_type_try_from<E, V>(
    xmltree: roxmltree::Children,
) -> Result<representation::BasicType<V>, E>
where
    E: From<V::Err>,
    V: FromStr,
{
    let mut label = None;
    let mut default_value = None;

    for child in xmltree {
        match child.tag_name().name() {
            "label" => label = child.text().map(String::from),
            "default" => {
                default_value = match child.text().map(V::from_str) {
                    Some(y) => Some(y?),
                    None => None,
                }
            }
            _ => continue,
        }
    }

    Ok(representation::BasicType::new(label, default_value))
}

impl<'a, 'input: 'a> TryFrom<roxmltree::Children<'a, 'input>> for representation::EnumEntry {
    type Error = KcfgSerializeError;

    fn try_from(xmltree: roxmltree::Children) -> Result<Self, Self::Error> {
        let mut label = None;
        let mut choices = representation::EnumChoices::default();

        for child in xmltree {
            match child.tag_name().name() {
                "label" => label = child.text().map(String::from),
                "choices" => choices = representation::EnumChoices::try_from(child.children())?,
                _ => continue,
            }
        }

        Ok(Self::new(choices, label))
    }
}

impl<'a, 'input: 'a> TryFrom<roxmltree::Children<'a, 'input>> for representation::EnumChoices {
    type Error = KcfgSerializeError;

    fn try_from(xmlnode: roxmltree::Children) -> Result<Self, Self::Error> {
        let mut default_value = None;
        let mut choices = Vec::new();
        for child in xmlnode {
            match child.tag_name().name() {
                "default" => default_value = child.text().map(String::from),
                "choice" => choices.push(representation::Choice::try_from(child)?),
                _ => continue,
            }
        }
        Ok(Self::new(default_value, choices))
    }
}

impl<'a, 'input: 'a> TryFrom<roxmltree::Node<'a, 'input>> for representation::Choice {
    type Error = KcfgSerializeError;

    fn try_from(xmlnode: roxmltree::Node) -> Result<Self, Self::Error> {
        let name = match xmlnode.attribute("name") {
            Some(attr) => attr.to_string(),
            None => {
                return Err(Self::Error::NameAttributeAbsent(
                    xmlnode.tag_name().name().to_string(),
                ))
            }
        };
        let label = match xmlnode.children().find(|x| x.tag_name().name() == "label") {
            Some(x) => x.text().map(String::from),
            None => None,
        };
        Ok(Self::new(name, label))
    }
}

#[cfg(test)]
mod tests {
    use crate::kcfg::representation::*;

    #[test]
    fn from_int_entry() {
        let xml = r###"
            <entry name="Width" type="Int">
                <label>Width of the main window.</label>
                <default>600</default>
            </entry>"###;
        let xmltree = roxmltree::Document::parse(xml).unwrap();
        let ans = KcfgEntry {
            name: "Width".to_string(),
            entry_type: EntryType::Int(BasicType::<i32> {
                label: Some("Width of the main window.".to_string()),
                default_value: Some(600),
            }),
        };
        assert_eq!(KcfgEntry::try_from(xmltree.root_element()).unwrap(), ans);
    }

    #[test]
    fn from_uint_entry() {
        let xml = r###"
            <entry name="Width" type="UInt">
                <label>Width of the main window.</label>
                <default>600</default>
            </entry>"###;
        let xmltree = roxmltree::Document::parse(xml).unwrap();
        let ans = KcfgEntry {
            name: "Width".to_string(),
            entry_type: EntryType::UInt(BasicType::<u32> {
                label: Some("Width of the main window.".to_string()),
                default_value: Some(600),
            }),
        };
        assert_eq!(KcfgEntry::try_from(xmltree.root_element()).unwrap(), ans);
    }

    #[test]
    fn from_int64_entry() {
        let xml = r###"
            <entry name="Width" type="Int64">
                <label>Width of the main window.</label>
                <default>600</default>
            </entry>"###;
        let xmltree = roxmltree::Document::parse(xml).unwrap();
        let ans = KcfgEntry {
            name: "Width".to_string(),
            entry_type: EntryType::Int64(BasicType::<i64> {
                label: Some("Width of the main window.".to_string()),
                default_value: Some(600),
            }),
        };
        assert_eq!(KcfgEntry::try_from(xmltree.root_element()).unwrap(), ans);
    }

    #[test]
    fn from_uint64_entry() {
        let xml = r###"
            <entry name="Width" type="UInt64">
                <label>Width of the main window.</label>
                <default>600</default>
            </entry>"###;
        let xmltree = roxmltree::Document::parse(xml).unwrap();
        let ans = KcfgEntry {
            name: "Width".to_string(),
            entry_type: EntryType::UInt64(BasicType::<u64> {
                label: Some("Width of the main window.".to_string()),
                default_value: Some(600),
            }),
        };
        assert_eq!(KcfgEntry::try_from(xmltree.root_element()).unwrap(), ans);
    }

    #[test]
    fn from_double_entry() {
        let xml = r###"
            <entry name="Width" type="Double">
                <label>Width of the main window.</label>
                <default>600</default>
            </entry>"###;
        let xmltree = roxmltree::Document::parse(xml).unwrap();
        let ans = KcfgEntry {
            name: "Width".to_string(),
            entry_type: EntryType::Double(BasicType::<f64> {
                label: Some("Width of the main window.".to_string()),
                default_value: Some(600.0),
            }),
        };
        assert_eq!(KcfgEntry::try_from(xmltree.root_element()).unwrap(), ans);
    }

    #[test]
    fn from_bool_entry() {
        let xml = r###"
            <entry name="Width" type="Bool">
                <label>Width of the main window.</label>
                <default>true</default>
            </entry>"###;
        let xmltree = roxmltree::Document::parse(xml).unwrap();
        let ans = KcfgEntry {
            name: "Width".to_string(),
            entry_type: EntryType::Bool(BasicType::<bool> {
                label: Some("Width of the main window.".to_string()),
                default_value: Some(true),
            }),
        };
        assert_eq!(KcfgEntry::try_from(xmltree.root_element()).unwrap(), ans);
    }

    #[test]
    fn from_enum_entry() {
        let xml = r###"
            <entry name="Platform" type="Enum">
                <label>Last operating system used.</label>
                <choices>
                    <choice name="Linux">
                        <label>Linux</label>
                    </choice>
                    <choice name="FreeBSD">
                        <label>FreeBSD</label>
                    </choice>
                    <choice name="Windows">
                        <label>Windows</label>
                    </choice>
                    <default>Linux</default>
                </choices>
            </entry>"###;
        let xmltree = roxmltree::Document::parse(xml).unwrap();
        let ans = KcfgEntry {
            name: "Platform".to_string(),
            entry_type: EntryType::Enum(EnumEntry {
                label: Some("Last operating system used.".to_string()),
                choices: EnumChoices {
                    default_value: Some("Linux".to_string()),
                    choices: vec![
                        Choice {
                            name: "Linux".to_string(),
                            label: Some("Linux".to_string()),
                        },
                        Choice {
                            name: "FreeBSD".to_string(),
                            label: Some("FreeBSD".to_string()),
                        },
                        Choice {
                            name: "Windows".to_string(),
                            label: Some("Windows".to_string()),
                        },
                    ],
                },
            }),
        };
        let parsed = KcfgEntry::try_from(xmltree.root_element()).unwrap();
        assert_eq!(parsed, ans);
    }

    #[test]
    fn from_group() {
        let xml = r###"
            <group name="General">
                <entry name="Width" type="UInt">
                    <label>Width of the main window.</label>
                    <default>600</default>
                </entry>
            </group>"###;
        let xmltree = roxmltree::Document::parse(xml).unwrap();
        let ans = Group {
            name: "General".to_string(),
            entries: vec![KcfgEntry {
                name: "Width".to_string(),
                entry_type: EntryType::UInt(BasicType::<u32> {
                    label: Some("Width of the main window.".to_string()),
                    default_value: Some(600),
                }),
            }],
        };
        assert_eq!(Group::try_from(xmltree.root_element()).unwrap(), ans);
    }

    #[test]
    fn from_kcfg() {
        let xml = r###"
            <?xml version="1.0" encoding="UTF-8"?>
            <kcfg xmlns="http://www.kde.org/standards/kcfg/1.0"
                  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                  xsi:schemaLocation="http://www.kde.org/standards/kcfg/1.0
                                      http://www.kde.org/standards/kcfg/1.0/kcfg.xsd" >
                <kcfgfile name="abc" />
                <group name="General">
                    <entry name="Width" type="Int">
                        <label>Width of the main window.</label>
                        <default>600</default>
                    </entry>
                </group>
            </kcfg>"###;
        let xmltree = roxmltree::Document::parse(xml.trim()).unwrap();
        let ans = Kcfg {
            file_name: "abc".to_string(),
            groups: vec![Group {
                name: "General".to_string(),
                entries: vec![KcfgEntry {
                    name: "Width".to_string(),
                    entry_type: EntryType::Int(BasicType::<i32> {
                        label: Some("Width of the main window.".to_string()),
                        default_value: Some(600),
                    }),
                }],
            }],
        };
        assert_eq!(Kcfg::try_from(xmltree.root_element()).unwrap(), ans);
    }
}
