pub mod kcfg_errrors;
pub mod kcfgc_errors;

use std::fmt::Debug;
use std::fmt::Display;

pub use kcfg_errrors::DeserializeError as KcfgDeserializeError;
pub use kcfg_errrors::SerializeError as KcfgSerializeError;
pub use kcfgc_errors::Error as KcfgcDeserializeError;

#[derive(Debug)]
pub enum Error {
    RoxmlError(roxmltree::Error),
    SerializerError(KcfgSerializeError),
    DeserializerError(KcfgDeserializeError),
}

impl From<roxmltree::Error> for Error {
    fn from(e: roxmltree::Error) -> Self {
        Self::RoxmlError(e)
    }
}

impl From<KcfgSerializeError> for Error {
    fn from(e: KcfgSerializeError) -> Self {
        Self::SerializerError(e)
    }
}

impl From<KcfgDeserializeError> for Error {
    fn from(e: KcfgDeserializeError) -> Self {
        Self::DeserializerError(e)
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::RoxmlError(x) => Display::fmt(&x, f),
            Self::SerializerError(x) => Display::fmt(&x, f),
            Self::DeserializerError(x) => Display::fmt(&x, f),
        }
    }
}
