use std::fmt::Display;

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Clone, Debug, PartialEq)]
pub enum Error {
    InvalidKcfgc,
    ExpectedBoolean(String),
    ExpectedString(String),
    InvalidTranslationSystem(String),
    InvalidMemberVariables(String),
    MissingKey(String),
}

impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::InvalidKcfgc => write!(f, "Provided Kcfgc file is not valid"),
            Self::ExpectedBoolean(x) => write!(f, "Expected boolean for {}", x),
            Self::ExpectedString(x) => write!(f, "Expected String for {}", x),
            Self::InvalidTranslationSystem(x) => write!(f, "Invalid translation System: {}", x),
            Self::InvalidMemberVariables(x) => {
                write!(f, "Invalid value for MemberVariables: {}", x)
            }
            Self::MissingKey(x) => write!(f, "Key {} mission in kcfgc", x),
        }
    }
}

impl std::error::Error for Error {}
