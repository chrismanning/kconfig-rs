# KConfig Crate for Rust

## Introduction
KConfig is persistent platform-independent application settings framework built by KDE. This
crate is a wrapper for the C++ framework and allows using it from Rust.
It can be used along with crates like [qmetaobject-rs](https://github.com/woboq/qmetaobject-rs)
to create QT applications entirely in Rust.

## Helpful Links about using KConfig
- [Using KConfig with Rust](https://www.programmershideaway.xyz/posts/post4/)
- [KDE Frameworks – Part 1](https://www.kdab.com/kde-frameworks-part-1-kconfig/)
- [KConfig C++ API](https://api.kde.org/frameworks/kconfig/html/index.html)
